@import url('http://fonts.cdnfonts.com/css/gilam');
@import url('http://fonts.cdnfonts.com/css/circe');

body{
    padding: 0;
    font-family: 'Gilam', sans-serif;
}

.content-container{
    width: 1460px;
    margin: 0 auto;
}

.site-container{
    background-color:#171717;
}

.site-container:first-child{
    background-image: url('../image/header-bg.jpeg');
    background-size:cover;
}

.site-container:nth-child(3){
    background-image: url('../image/parallax-bg.jpeg');
    background-size:cover;
}

header{
    padding-top:40px;
}

.row-wrapper-content{
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
}

.logo{
    width: 216px;
    height: 33px;
}

.button{
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    font-family: 'Circe', sans-serif;
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 30px;
    color: #E4B564;
    background-color: rgba(28,28,28,0);
    border: 1px solid #E4B564;
}

.button.header{
    width: 218px;
    height: 50px;
}

.phone{
    Width: 12.96px;
    height: 12.96px;
    margin-right: 10px;
}

.company-description{
    width: 1182px;
    font-style: normal;
    font-weight: 700;
    font-size: 20px;
    line-height: 28px;
    color: #FFFFFF;
    margin-top: 40px;
    margin-bottom: 41px;
}

.subtitle{
    font-style: normal;
    font-weight: 700;
    font-size: 50px;
    line-height: 28px;
    color: #FFFFFF;
}

.subtitle.header{
    padding-top: 128px;
}

.container-heating-networks{
    display: grid;
    grid-template-columns: 468px repeat(2,220px) 468px;
    grid-template-rows: repeat(2, 468px);
    grid-gap: 28px 30px;
    padding-bottom: 33px;
}

.grid-item-heating-networks{
    display: flex;
    justify-content: center;
    align-items: center;
    font-style: normal;
    font-weight: 700;
    font-size: 20px;
    line-height: 28px;
    text-align: center;
    color: #FFFFFF;
}

.grid-item-heating-networks:nth-child(1){
    background-color: #634F2C;
}

.grid-item-heating-networks:nth-child(2){
    grid-column: span 2;
    background-color: #B4A385;
    padding-left: 44px;
    padding-right: 44px;
}

.grid-item-heating-networks:nth-child(3){
    background-color: #E4B564;
}

.grid-item-heating-networks:nth-child(4){
    grid-column: span 2;
    background-color: #635A4A;
}

.grid-item-heating-networks:nth-child(5){
    grid-column: span 2;
    background-color: #B08C4D;
}

.subtitle.main{
    padding-top: 120px;
}

.container-construction-machinery{
    display: grid;
    grid-template-columns: repeat(3, 467px);
    height: 898px;
    grid-gap: 29.5px;
    margin-top: 94px;
    padding-bottom: 204px;
}

.column1-construction-machinery{
    display: grid;
    grid-template-rows: 313px 560px;
    grid-gap: 24px;
}

.column2-construction-machinery{
    display: grid;
    grid-template-rows: 439px 405px;
    grid-gap: 33px;
}

.column3-construction-machinery{
    display: grid;
    grid-template-rows: 255px 313px 277px;
    grid-gap: 26px;
}

.grid-item-construction-machinery{
    font-style: normal;
    font-weight: 400;
    font-size: 20px;
    line-height: 25px;
    color: #FFFFFF;
}

.grid-item-construction-machinery.column1{
    background-color: #0E0E0E;
    padding-top: 44px;
    padding-left: 52px;
}

.grid-item-construction-machinery.column1:nth-child(2){
    background-color: #1F1F1F;
    padding-top: 56px;
    padding-left: 52px;
    font-weight: 500;
}

.grid-item-construction-machinery.column2{
    background-color: #242424;
    padding-top: 44px;
    padding-left: 51px;
}

.grid-item-construction-machinery.column2:nth-child(2){
    background-color: #E4B564;
    padding-top: 47px;
    padding-left: 52px;
    color:#000000;
    font-weight: 500;
}

.grid-item-construction-machinery.column3{
    background-color: #1F1F1F;
    padding-top: 44px;
    padding-left: 48px;
    font-weight: 500;
}

.grid-item-construction-machinery.column3:nth-child(2){
    background-color: #0E0E0E;
    padding-top: 44px;
    padding-left: 48px;
    font-weight: 500;
}

.grid-item-construction-machinery.column3:nth-child(3){
    background-color: #0A0909;
    padding-top: 44px;
    padding-left: 48px;
    font-weight: 700;
}

footer{
    padding-top:161px;
}

.button.footer{
    width: 180px;
    height: 50px;
}

.flex-container{
    width: 1460px;
    height: auto;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin-top: 61px;
    padding-bottom: 226px;
}

.item-flex-container{
    display: flex;
    align-items: center;
    justify-content: center;
    width: 487px;
    height: 391px;
    background-color: white;
}

.item-flex-container:first-child,.item-flex-container:last-child{
    background-color: #634F2C;
}

.item-flex-container:nth-child(2){
    flex-direction: column;
    align-items: center;
    width: 486px;
    background: rgba(232, 210, 171, 0.76);
}

.item-flex-container:nth-child(3){
    background-color: #E4B564;
}

.item-flex-container:nth-child(4){
    background-color: #635A4A;
}

.item-flex-container:nth-child(5){
    width: 486px;
    background-color: #B08C4D;
}

.eid-logo{
    width: 185px;
    height: 49px;
}

.sphere-img{
    width: 62px;
    height: 55px;
}

.mavis-logo{
    width: 139px;
    height: 55px;
}

.text-mavis{
    width: 326px;
    height: 140px;
    margin-top: 16px;
    font-style: normal;
    font-weight: 600;
    font-size: 22px;
    line-height: 28px;
    text-align: center;
    color: #FFFFFF;
}

.arsenal-logo{
    width: 190px;
    height: 42px;
}

.setgroup-logo{
    width: 119px;
    height: 53px;
}






