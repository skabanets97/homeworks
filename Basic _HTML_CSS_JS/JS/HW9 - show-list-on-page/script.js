/*
    Теоретичні питання

    1. Опишіть, як можна створити новий HTML тег на сторінці.
       За допомогою метода document.createElement(tag) можно створює елемент із заданим тегом.

    2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
       elem.insertAdjacentHTML(where, html)
       Перший параметр що вказує, куди по відношенню до elem робити вставку.
       Він може приймати такі значення:
       - beforebegin (вставити html безпосередньо перед elem)
       - afterbegin (вставити html на початок elem)
       - beforeend (вставити html в кінець elem)
       - afterend (вставити html безпосередньо після elem)

    3. Як можна видалити елемент зі сторінки?
       Метод Element.remove() видаляє елемент із DOM-дерева, в якому він знаходиться.
 */

const arr = ["Kharkiv", "Kiev", "Odessa", "Lviv", "Dnieper"];

function createList(arr){
    const list = document.createElement('ul');
    document.body.append(list);
    arr.forEach((item, element) => {
        element = document.createElement('li');
        element.innerText = item;
        list.append(element);
    });
    timer();
}

function timer(){
    window.setTimeout(clearPage, 3000);
}

function clearPage() {
    document.body.style.display = "none";
}

createList(arr);
