/*
    Теоретический вопрос

    1) Почему для работы с input не рекомендуется использовать события клавиатуры?
    События клавиатуры предназначены именно для работы с клавиатурой.
    Их можно использовать для проверки ввода в <input>, но будут недочёты.
    Например, текст может быть вставлен мышкой, при помощи правого клика
    и меню, без единого нажатия клавиши.
 */

const buttonsWrapper = document.querySelector('.btn-wrapper');
const buttons = [...buttonsWrapper.children];

document.addEventListener('keypress', e => {
    console.log(e.key);

    buttons.forEach( button => {

        let textButton = button.textContent;
        //подсвечивает кнопку без учета регистра
        if(e.key === textButton || e.key === textButton.toUpperCase() || e.key === textButton.toLowerCase()){
            buttons.forEach( btn => {
                btn.classList.remove('active');
            })
            button.classList.add('active');
        }
    })
});