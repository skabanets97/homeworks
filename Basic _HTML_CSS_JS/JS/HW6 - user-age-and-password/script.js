/*
    Теоретичні питання

    1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
       Екранування полягає в додаванні спецсимволів перед символами або рядками для запобігання
       їх некоректній інтерпретації, наприклад, додавання символу \ перед "(подвійними лапками)
       дозволяє інтерпретувати їх як частину тексту, а не як позначення закінчення рядка.

    2. Які засоби оголошення функцій ви знаєте?
       Існує три способи оголошення функції:
       - Function Declaration
       - Function Expression
       - Named Function Expression

    3. Що таке hoisting, як він працює для змінних та функцій?
       Hoisting - це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості
       перед виконанням коду.
       
 */

function createNewUser() {
    let newUser = {
        firstName: prompt('Введите имя'),
        lastName: prompt('Введите фамилию'),
        birthday: Date.parse(prompt('Введите дату рождения', 'YYYY-MM-DD')),
        getLogin (){
            console.log((this.firstName.trim()[0] + this.lastName).toLowerCase());
        },
        getAge (){
            const birthDate = new Date(this.birthday);
            const currentDate = new Date();
            const years = (currentDate.getFullYear() - birthDate.getFullYear());
            console.log(years);
        },
        getPassword(){
            const dateForPass = new Date(this.birthday);
            console.log(this.firstName.trim()[0] + this.lastName.toLowerCase() + dateForPass.getFullYear());
        }
    };
    console.log(newUser);
    newUser.getLogin();
    newUser.getAge();
    newUser.getPassword();
    return newUser;
}

createNewUser();


