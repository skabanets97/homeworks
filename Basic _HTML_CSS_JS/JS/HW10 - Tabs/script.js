const tabsList = document.querySelector('.tabs');
const tabsTitle = [...tabsList.children];
const tabsContent = document.querySelector('.tabs-content');
const tabsContentElement = [...tabsContent.children];

function setDataAtr (titles, elementsContent) {
    titles.forEach(item => {
        item.setAttribute('data-tabs', `${item.innerText}`);
    });

    elementsContent.forEach( (item, index) => {
        item.classList.add('tabs-content-element');
        item.setAttribute('data-tabs', `${titles[index].getAttribute('data-tabs')}`);
    });

    elementsContent[0].classList.add('active');
}

setDataAtr(tabsTitle, tabsContentElement);

tabsList.addEventListener('click', event => {
    if (event.target !== tabsList){
        tabsTitle.forEach( item => {
            item.classList.remove('active');
        });
    }

    tabsContentElement.forEach( contentTab => {
        if (contentTab.dataset.tabs === event.target.dataset.tabs) {
            contentTab.classList.add('active');
        } else {
            contentTab.classList.remove('active');
        }
    });

    event.target.classList.toggle('active');
});



