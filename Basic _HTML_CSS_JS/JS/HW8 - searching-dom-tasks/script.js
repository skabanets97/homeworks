/*
    Теоретичні питання

    1. Опишіть своїми словами що таке Document Object Model (DOM)
    DOM — це незалежний від платформи та мови програмний інтерфейс, що дозволяє програмам
    та скриптам отримати доступ до вмісту HTML, XHTML та XML документів,
    а також змінювати вміст, структуру та оформлення таких документів.

    2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    innerText - показує весь текстовий вміст, який не відноситься до синтаксису HTML.
    Тобто будь-який текст, укладений між відкриваючими за закриваючими тегами елемента, буде записаний в innerText.
    innerHTML - покаже текстову інформацію рівно по одному елементу. У вивод потрапить текст та розмітка HTML-документа,
    яка може бути укладена між відкриваючими і закриваючими тегами основного елемента.

    3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

    До елементів верхнього рівня можна звернутися безпосередньо, використовуючи властивості об'єкта document:
    - document.documentElement - для звернення до елементу
    - document.head - для звернення до елементу
    - document.body - для звернення до елементу
    Для звернення до будь-якого елементу сторінки в DOM інтерфейсі введені спеціальні пошукові методи:
    - getElementById () - виконує пошук елемента за його унікальним ідентифікатором id
    - getElementsByName () - пошук ведеться по всіх елементах сторінки із заданим атрибутом name
    - getElementsByTagName () - метод виконує пошук по тегу
    - getElementsByClassName () - метод виконує пошук по глобальному атрибуту class
    - querySelectorAll () - метод повертає список всіх елементів, що задовольняють заданій CSS-селектору
    - querySelector () - метод повертає перший елемент, що задовольняє заданому CSS-селектору
*/

// 1. Найти все параграфы на странице и установить цвет фона #ff0000

const paragraphs = Array.from(document.getElementsByTagName('p'));

paragraphs.forEach(changeColorParagraphs);

function changeColorParagraphs (element) {
    element.style.backgroundColor = '#ff0000';
}

// 2. Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.

const findElement = document.getElementById("optionsList");
console.log(findElement, '- element with id="optionsList"');

console.log(findElement.parentNode, '- element parent');

const children = Array.from(findElement.children);
console.log(children, '- children of the element');

// 3. Установите в качестве контента элемента с классом testParagraph следующий параграф
// This is a paragraph

const changeText = document.querySelector('#testParagraph');
changeText.textContent = 'This is a paragraph';

// 4. Получить элементы, вложенные в элемент с классом main-header и вывеcти их в консоль.
// Каждому из элементов присвоить новый класс nav-item.

const mainElement = document.querySelector('.main-header');
const elementsMainElement = Array.from(mainElement.children);

elementsMainElement.forEach(addNewClass);

function addNewClass (element){
    element.classList.add("nav-item");
}
console.log(elementsMainElement);


//5. Найти все элементы с классом section-title. Удалить этот класс у элементов.

const elementsSectionTitle = Array.from(document.querySelectorAll('.section-title'));

elementsSectionTitle.forEach(removeClass);

function removeClass (element) {
    element.classList.remove("section-title");
}

console.log(elementsSectionTitle);