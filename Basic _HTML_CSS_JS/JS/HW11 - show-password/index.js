'use strict'

const form = document.querySelector('.password-form');
const firstPasswordInput = document.querySelector('.first-password');
const secondPasswordInput = document.querySelector('.second-password');
const firstIconShow = document.querySelector('#first-icon-show');
const firstIconHide = document.querySelector('#first-icon-hide');
const secondIconShow = document.querySelector('#second-icon-show');
const secondIconHide = document.querySelector('#second-icon-hide');
const message = document.querySelector('#message');

function showPassword (password, iconShow, iconHide) {

    iconShow.addEventListener('click', e => {
        password.setAttribute('type', 'text');
        iconShow.classList.toggle('invisible');
        iconHide.classList.toggle('invisible');
    });

    iconHide.addEventListener('click', e => {
        password.setAttribute('type', 'password');
        iconHide.classList.toggle('invisible');
        iconShow.classList.toggle('invisible');
    });
}

showPassword(firstPasswordInput, firstIconShow, firstIconHide);
showPassword(secondPasswordInput, secondIconShow, secondIconHide);

function verificationPasswords (firstPassword, secondPassword, error) {
    error.innerHTML = '';

    if(firstPassword.value.length < 8){
        error.innerHTML = 'Password must not be less than 8 characters';
    }

    let lowerCaseLetters = /[a-z]/g;
    if(!firstPassword.value.match(lowerCaseLetters)){
        error.innerHTML = 'Password must contain at least one lowercase letter';
    }

    let upperCaseLetters = /[A-Z]/g;
    if(!firstPassword.value.match(upperCaseLetters)){
        error.innerHTML = 'Password must contain at least one capital letter';
    }

    let numbers = /[0-9]/g;
    if(!firstPassword.value.match(numbers)){
        error.innerHTML = 'Password must contain at least one number';
    }

    let specialSymbols = /[!@#$%^&*]/g;
    if(!firstPassword.value.match(specialSymbols)){
        error.innerHTML = 'Password must contain at least one special symbols';
    }

    if(firstPassword.value !== secondPassword.value){
        alert('Passwords must be the same!');
    }
}

form.addEventListener('submit', e => {
    e.preventDefault();
    verificationPasswords(firstPasswordInput, secondPasswordInput, message);
    console.log(firstPasswordInput.value, secondPasswordInput.value);
});

