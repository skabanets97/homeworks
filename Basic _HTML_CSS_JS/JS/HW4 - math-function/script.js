/*
Теоретичні питання

1. Описати своїми словами навіщо потрібні функції у програмуванні.
   Функції потрібні, щоб помітно спрощувати та скорочувати код,
   адаптувати його для різних платформ, легко налагоджувати.

2. Описати своїми словами, навіщо у функцію передавати аргумент.
   Аргумент – це значення, яке передається функції під час її виклику.
   Аргумент потрібен щоб функція прийняла та обробила певні передані дані,
   та повернула результат.

3. Що таке оператор return та як він працює всередині функції?
   При виклику оператора return виконання функції припиняється.
   Цей оператор повертає вказане значення до місця виклику функції.
 */

let firstNumber = Number(prompt('Введите первое число'));
let secondNumber = Number(prompt('Введите второе число'));
let operation = prompt('Введите математическую операцию ( +, -, *, /)');

const inputValidation = () => {
    if (!firstNumber || isNaN(firstNumber)){
        firstNumber = Number(prompt('Ошибка ввода! Введите первое число еще раз!'));
        inputValidation();
    } else if (!secondNumber || isNaN(secondNumber)){
        secondNumber = Number(prompt('Ошибка ввода! Введите второе число еще раз!'));
        inputValidation();
    } else if((operation !== '+') && (operation !== '-') && (operation !== '*') && (operation !== '/')){
        operation = prompt('Ошибка ввода! Введите математическую операцию еще раз ( +, -, *, /)');
        inputValidation();
    }
}

function calculation(firstNumber, secondNumber, operation){
    if(operation === '+'){
        return firstNumber + secondNumber;
    } else if(operation === '-'){
        return firstNumber - secondNumber;
    } else if(operation === '*'){
        return firstNumber * secondNumber;
    } else if(operation === '/'){
        return firstNumber / secondNumber;
    }
}

inputValidation();
console.log(`Результат: ${firstNumber} ${operation} ${secondNumber} = ${calculation(firstNumber,secondNumber, operation)}`);