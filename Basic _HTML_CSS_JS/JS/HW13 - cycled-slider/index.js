/*
    Теоретичні питання

    1) Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
       Різниця у тому, що setTimeout() відпрацьовує рівно один раз, тобто. викликає через зазначений інтервал
       зазначену функцію та зупиняє свою роботу. Друга функція ж, setInterval() приймає такі самі параметри,
       але викликавши функцію одного разу, вона продовжує робити це знову і знову через однакові проміжки часу.
       Зупинити її можна лише за допомогою функції clearInterval.

    2) Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
       setTimeout() з затримкою 0c - функція буде виконуватись відразу після виконання поточного коду.

    3) Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
       Оскільки setInterval() виконує функцію періодично без зупинки він буде впливати на наступний код.
       Тому його відключають за допомогою clearInterval().
 */


const srcImages = ['./img/1.jpg', './img/2.jpg', './img/3.jpg', './img/4.png'];
const image = document.querySelector('.img');
const count = document.querySelector('#count');
const btnStart = document.querySelector('.btn.start');
const btnStop = document.querySelector('.btn.stop');

let COUNT_S = 3;
let COUNT_MS = 0;
let counterImg = 0;
let timer;
let clickerStart = 0;

image.src = srcImages[0];

btnStart.addEventListener('click', e => {

    if(clickerStart >= 1){
        return;
    }

    timer = setInterval( () => {
        srcImages.forEach( () => {
            count.innerText = `${COUNT_S}:${COUNT_MS}`;

            if(COUNT_MS === 0){
                COUNT_S--;
                COUNT_MS = 99;

                if (COUNT_S === -1) {
                    counterImg++;
                    COUNT_S = 3;
                    COUNT_MS = 0;
                    image.src = srcImages[counterImg % 4];
                }
            }

        });
        COUNT_MS--;
    }, 10);
    clickerStart++;
});

btnStop.addEventListener('click', e => {
    clearInterval(timer);
    clickerStart = 0;
})





