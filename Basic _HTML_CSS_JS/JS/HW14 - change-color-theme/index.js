const buttonSwitch = document.querySelector('.button.standard.switch');
const buttons = document.querySelectorAll('.button.standard');
const formLinks = document.querySelectorAll('.form-link');
const square = document.querySelector('.square');
const form = document.querySelector('.form-wrapper');
const headerLinks = document.querySelectorAll('.header-link');
const svg = document.querySelectorAll('.svg-header');
const elementsStyle = [...buttons, ...formLinks, square, form, ...headerLinks, ...svg];

function switchTheme (elements) {
    elements.forEach ( item => {

        if(!item.classList.contains('new-style')){
            item.classList.add('new-style');
            localStorage.setItem('Style', 'new');
        } else{
            item.classList.remove('new-style');
            localStorage.setItem('Style', 'default');
        }
    });
}

buttonSwitch.addEventListener('click', e => {
    switchTheme(elementsStyle);
});


function checkKey (elements) {
    elements.forEach( item => {
        let itemKey = localStorage.getItem(`Style`);
        if (itemKey === 'new'){
            item.classList.add('new-style');
        } else {
            item.classList.remove('new-style');
        }
    })
}

checkKey(elementsStyle);