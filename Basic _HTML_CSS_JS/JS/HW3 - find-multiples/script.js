/*
Теоретичні питання

1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
   У програмуванні цикли застосовуються для того, щоб повторити деяку дію потрібну кількість разів,
   доки певна умова залишається істинною. Наприклад, відсортувати елементи масиву або знайти факторіал числа.

2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
   - Цикл while (Код з тіла циклу виконується, поки умова умова істинна)
   - Цикл do-while (Цикл спочатку виконає тіло, а потім перевірить умову,
   і доки його значення дорівнює true, він буде виконуватися знову і знову.
   Особливість цього циклу в тому що тіло циклу виконаеться принайні 1 раз)
   - Цикл for (Початок виконується один раз, а потім кожна ітерація
   полягає у перевірці умови, після якої виконується тіло та крок).

   Цикл while застосовують коли потрібно виконувати якісь дії, доки умова не буде істина.
   Цикл do-while застосовують коли потрібно щоб тіло циклу винонолось принайні один раз.
   Цикл for застосовують щоб перебрати чи відсортувати значення в заданному діапазоні.

3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
   Приведення типів - це процес перетворення значень з одного типу в інший
   (наприклад - рядки в число, об'єкта - в логічне значення ).
   Коли ми хочемо сконвертувати значення одного типу в значення іншого типу,
   записуючи це відповідним чином у коді, це називається явним приведенням типів.
   JavaScript - це мова зі слабкою типізацією, значення можуть бути конвертовані
   між різними типами автоматично. Це і є неявним приведенням типів.

 */

let m = Number(prompt('Enter the start of the range'));
let n = Number(prompt('Enter the end of the range'));

const findSimpleNumbers = () =>{

    inputValidation();

    console.log(`Simple numbers in the range ${m} - ${n}`);
    for(let i = m; i < n+1; i++){
        let del = 0;
        for(let j = 0; j <= n; j++) {
            if (i % j === 0)  {
                del++;
            }
        }
        if(del === 2){
            console.log(i);
        }
    }
}

const  inputValidation = () => {

    if(!m || isNaN(m)){
        m = Number(prompt('Invalid input! Enter the start of the range again!'));
        inputValidation();
    }

    else if(!n || isNaN(n)) {
        n = Number(prompt('Invalid input! Enter the end of the range again!'));
        inputValidation();
    }

    else if ( m >= n){
        alert('Incorrect range entered!');
        m = Number(prompt('Invalid input! Enter the start of the range again!'));
        n = Number(prompt('Invalid input! Enter the end of the range again!'));
        inputValidation();
    }

    else if(!(Number.isInteger(m)) || !(Number.isInteger(n))){
        alert('You can only enter whole numbers!');
        m = Number(prompt('Invalid input! Enter the start of the range again!'));
        n = Number(prompt('Invalid input! Enter the end of the range again!'));
        inputValidation();
    }

}

findSimpleNumbers();